const mapObject = require('../mapObject.js');

// const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const testObject = { start: 5, end: 12 };

const cb = function (value) {
  return value + 10;
};

const result = mapObject(testObject, cb);

console.log(result);
