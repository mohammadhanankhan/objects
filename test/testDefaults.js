const defaults = require('../defaults');

const iceCream = { flavor: 'chocolate' };
const defaultProps = { flavor: 'vanilla', sprinkles: 'lots' };

const result = defaults(iceCream, defaultProps);

console.log(result);
