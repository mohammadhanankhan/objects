// Convert an object into a list of [key, value] pairs.
// http://underscorejs.org/#pairs
function pairs(obj) {
  const pairs = Object.entries(obj);

  return pairs;
}

module.exports = pairs;
