// Return all of the values of the object's own properties.
// Ignore functions
// http://underscorejs.org/#values

function values(obj) {
  const valuesArr = [];

  for (let prop in obj) {
    if (obj.hasOwnProperty(prop) && typeof obj[prop] !== 'function') {
      valuesArr.push(obj[prop]);
    }
  }

  return valuesArr;
}

module.exports = values;
