// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.
// http://underscorejs.org/#defaults

// Example:
// var iceCream = {flavor: "chocolate"};
// _.defaults(iceCream, {flavor: "vanilla", sprinkles: "lots"});
// => {flavor: "chocolate", sprinkles: "lots"}
function defaults(obj, defaultProps) {
  for (let prop in defaultProps) {
    if (!obj.hasOwnProperty(prop)) {
      obj[prop] = defaultProps[prop];
    }
  }

  return obj;
}

module.exports = defaults;
