// Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
// http://underscorejs.org/#mapObject
function mapObject(obj, cb) {
  const mappedObj = {};

  for (let prop in obj) {
    const mappedValue = cb(obj[prop]);

    mappedObj[prop] = mappedValue;
  }

  return mappedObj;
}

module.exports = mapObject;
